import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import CounterOptionView from '@/components/CounterOption.vue'

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/counter",
    name: "counter",
    component: CounterOptionView,
  },
  {
    path: "/counter-api",
    name: "counter-api",
    component: () => import('../components/CounterCompo.vue'),
  },
  {
    path: '/about',
    name: 'about',
    component: 
    () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
